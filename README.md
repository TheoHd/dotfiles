# dotfiles

## Setup

You can setup the main files by executing as following:

```shell
sh [environment]_quickstart.sh
```

If Docker is used in the targetted environment, [create a Docker ID here](https://hub.docker.com/signup). Your account will be required to execute quick start such as ```debian_quickstart.sh```.